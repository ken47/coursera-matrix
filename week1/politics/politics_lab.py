voting_data = list(open("voting_record_dump109.txt"))


## Task 1

def create_voting_dict():
    """
    Input: None (use voting_data above)
    Output: A dictionary that maps the last name of a senator
            to a list of numbers representing the senator's voting
            record.
    Example: 
        >>> create_voting_dict()['Clinton']
        [-1, 1, 1, 1, 0, 0, -1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1, 1]

    This procedure should return a dictionary that maps the last name
    of a senator to a list of numbers representing that senator's
    voting record, using the list of strings from the dump file (strlist). You
    will need to use the built-in procedure int() to convert a string
    representation of an integer (e.g. '1') to the actual integer
    (e.g. 1).

    You can use the split() procedure to split each line of the
    strlist into a list; the first element of the list will be the senator's
    name, the second will be his/her party affiliation (R or D), the
    third will be his/her home state, and the remaining elements of
    the list will be that senator's voting record on a collection of bills.
    A "1" represents a 'yea' vote, a "-1" a 'nay', and a "0" an abstention.

    The lists for each senator should preserve the order listed in voting data. 
    """
    ret = {}
    for item in voting_data:
      item0 = item.split()
      lastName = item0[0]
      votingRec = item0[3:]
      for key,vote in enumerate(votingRec):
        votingRec[key] = int(vote)
      ret[lastName] = votingRec    
    
    return ret

def getD():
    ret = []
    for item in voting_data:
      item0 = item.split()
      if item0[1] == 'D':
        ret.append(item0[0])
    return ret
    

## Task 2

def policy_compare(sen_a, sen_b, voting_dict):
    """
    Input: last names of sen_a and sen_b, and a voting dictionary mapping senator
           names to lists representing their voting records.
    Output: the dot-product (as a number) representing the degree of similarity
            between two senators' voting policies
    Example:
        >>> voting_dict = {'Fox-Epstein':[-1,-1,-1,1],'Ravella':[1,1,1,1]}
        >>> policy_compare('Fox-Epstein','Ravella', voting_dict)
        -2
    """
    comparison = 0
    for vote in range(len(voting_dict[sen_a])):
      comparison += voting_dict[sen_a][vote] * voting_dict[sen_b][vote]

    return comparison


## Task 3

def most_similar(sen, voting_dict):
    """
    Input: the last name of a senator, and a dictionary mapping senator names
           to lists representing their voting records.
    Output: the last name of the senator whose political mindset is most
            like the input senator (excluding, of course, the input senator
            him/herself). Resolve ties arbitrarily.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> most_similar('Klein', vd)
        'Fox-Epstein'

    Note that you can (and are encouraged to) re-use you policy_compare procedure.
    """
    match = ''
    highest_similarity = 0
    for senB, votes in voting_dict.items():
      if senB == sen:
        continue
      comparison = policy_compare(sen, senB, voting_dict)
      if comparison > highest_similarity:
        highest_similarity = comparison
        match = senB
    return match
    

## Task 4

def least_similar(sen, voting_dict):
    """
    Input: the last name of a senator, and a dictionary mapping senator names
           to lists representing their voting records.
    Output: the last name of the senator whose political mindset is least like the input
            senator.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> least_similar('Klein', vd)
        'Ravella'
    """
    match = ''
    lowest_similarity = float("inf")
    for senB, votes in voting_dict.items():
      comparison = policy_compare(sen, senB, voting_dict)
      if comparison < lowest_similarity:
        lowest_similarity = comparison
        match = senB
    return match
    
    

## Task 5

most_like_chafee    = most_similar('Chafee', create_voting_dict())
least_like_santorum = least_similar('Santorum', create_voting_dict()) 



# Task 6

def find_average_similarity(sen, sen_set, voting_dict):
    """
    Input: the name of a senator, a set of senator names, and a voting dictionary.
    Output: the average dot-product between sen and those in sen_set.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> find_average_similarity('Klein', {'Fox-Epstein','Ravella'}, vd)
        -0.5
    """
    tot = 0
    
    for senB in sen_set:
      dot = policy_compare(sen, senB, voting_dict)
      tot += dot

    return tot / len(sen_set)

def getAvgDem():
  dems = getD()
  highest_sim = 0
  match = ''
  for dem in dems:
    sim = find_average_similarity(dem, dems, create_voting_dict())
    if sim > highest_sim:
      highest_sim = sim
      match = dem
  return match

most_average_Democrat = getAvgDem()


# Task 7

def find_average_record(sen_set, voting_dict):
    """
    Input: a set of last names, a voting dictionary
    Output: a vector containing the average components of the voting records
            of the senators in the input set
    Example: 
        >>> voting_dict = {'Klein': [-1,0,1], 'Fox-Epstein': [-1,-1,-1], 'Ravella': [0,0,1]}
        >>> find_average_record({'Fox-Epstein','Ravella'}, voting_dict)
        [-0.5, -0.5, 0.0]
    """
    for sen in sen_set:
      tot = [0 for x in range(len(voting_dict[sen]))]
      break

    for sen in sen_set:
      for key, val in enumerate(voting_dict[sen]):
        tot[key] += val

    for key, val in enumerate(tot):
      tot[key] = val / len(sen_set)

    return tot

def getAvgDem2():
  highest_sim = 0
  dems = getD()
  voting_dict = create_voting_dict()
  voting_dict['avgDem'] = find_average_record(dems, voting_dict)
  for dem in dems:
    sim = policy_compare(dem, 'avgDem', voting_dict)
    if sim > highest_sim:
      highest_sim = sim
      match = dem
  return match

average_Democrat_record = find_average_record(getD(), create_voting_dict()) # (give the vector)


# Task 8

def bitter_rivals(voting_dict):
    """
    Input: a dictionary mapping senator names to lists representing
           their voting records
    Output: a tuple containing the two senators who most strongly
            disagree with one another.
    Example: 
        >>> voting_dict = {'Klein': [-1,0,1], 'Fox-Epstein': [-1,-1,-1], 'Ravella': [0,0,1]}
        >>> bitter_rivals(voting_dict)
        ('Fox-Epstein', 'Ravella')
    """
    min_sim = float("inf")
    rivals = (0, 0)
    for key, val in voting_dict.items():
      for key2, val2 in voting_dict.items():
        if key == key2:
          continue

        sim = policy_compare(key, key2, voting_dict)
        if sim < min_sim:
          min_sim = sim
          rivals = (key, key2)

    return rivals
