from vec import Vec
from GF2 import one
from itertools import combinations

D = {0,1,2,3,4,5,6}

def makeVec(vals):
    counter = 0
    f = {}
    for v in vals:
        f[counter] = v
        counter += 1
    return Vec(D, f)

a = makeVec([one,one,0,0,0,0,0])
b = makeVec([0,one,one,0,0,0,0])
c = makeVec([0,0,one,one,0,0,0])
d = makeVec([0,0,0,one,one,0,0])
e = makeVec([0,0,0,0,one,one,0])
f = makeVec([0,0,0,0,0,one,one])

vecs = [a,b,c,d,e,f]

def findSet(target):
    for i in D:
        combos = combinations(range(6), i+1)
        for combo in combos:
            #print(combo)
            match = True
            tot = makeVec({0,0,0,0,0,0,0})
            for key in combo:
                tot += vecs[key]
            #print(tot.f.values(), target)
            for i in D:
                if target[i] != tot.f[i]:
                   match = False
                   break
            if match:
                return combo

    return {}

result = findSet([0,0,one,0,0,one,0])
print(result)

result = findSet([0,one,0,0,0,one,0])
print(result)
