from GF2 import one
from prob4 import makeVec

combos = []
def genCombos(combo):
    if len(combo) == 4:
        combos.append(combo)
        return
    genCombos(combo + [0])
    genCombos(combo + [one])

genCombos([])

a = makeVec([one,one,0,0])
b = makeVec([one,0,one,0])
c = makeVec([one,one,one,one])
for combo in combos:
    test = makeVec(combo)
    if a*test == one and b*test == one and c*test == one:
        print(combo)
print('end')
