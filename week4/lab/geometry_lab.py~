from mat import *
from matutil import *
from math import *

## Task 1
def identity(labels = {'x','y','u'}):
    '''
    In case you have never seen this notation for a parameter before,
    the way it works is that identity() now defaults to having labels 
    equal to {'x','y','u'}.  So you should write your procedure as if 
    it were defined 'def identity(labels):'.  However, if you want the labels of 
    your identity matrix to be {'x','y','u'}, you can just call 
    identity().  Additionally, if you want {'r','g','b'}, or another set, to be the
    labels of your matrix, you can call identity({'r','g','b'}).  
    '''
    d = {}
    for l in labels:
      d[(l,l)] = 1

    return Mat((labels, labels), d)

## Task 2
def translation(x,y):
    '''
    Input:  An x and y value by which to translate an image.
    Output:  Corresponding 3x3 translation matrix.
    '''
    D = {'x','y','u'}
    f = {}
    f[('x','x')] = 1
    f[('y','y')] = 1
    f[('u','u')] = 1
    f[('x','u')] = x
    f[('y','u')] = y
    return Mat((D,D), f)

## Task 3
def scale(a, b):
    '''
    Input:  Scaling parameters for the x and y direction.
    Output:  Corresponding 3x3 scaling matrix.
    '''
    D = {'x','y','u'}
    f = {}
    f[('x','x')] = a
    f[('y','y')] = b
    f[('u','u')] = 1
    return Mat((D,D), f)

## Task 4
def rotation(angle):
    '''
    Input:  An angle in radians to rotate an image.
    Output:  Corresponding 3x3 rotation matrix.
    Note that the math module is imported.
    '''
    D = {'x','y','u'}
    f = {}
    f[('x','x')] = cos(angle)
    f[('x','y')] = -1 * sin(angle)
    f[('y','y')] = cos(angle)
    f[('y','x')] = sin(angle)
    f[('u','u')] = 1
    return Mat((D,D), f)

## Task 5
def rotate_about(x,y,angle):
    '''
    Input:  An x and y coordinate to rotate about, and an angle
    in radians to rotate about.
    Output:  Corresponding 3x3 rotation matrix.
    It might be helpful to use procedures you already wrote.
    '''
    return translation(x,y) * rotation(angle) * translation(-x,-y)

## Task 6
def reflect_y():
    '''
    Input:  None.
    Output:  3x3 Y-reflection matrix.
    '''
    D = {'x','y','u'}
    f = {}
    f[('x','x')] = -1
    f[('y','y')] = 1
    f[('u','u')] = 1
    return Mat((D,D), f)

## Task 7
def reflect_x():
    '''
    Inpute:  None.
    Output:  3x3 X-reflection matrix.
    '''
    D = {'x','y','u'}
    f = {}
    f[('x','x')] = 1
    f[('y','y')] = -1
    f[('u','u')] = 1
    return Mat((D,D), f)

    
## Task 8    
def scale_color(scale_r,scale_g,scale_b):
    '''
    Input:  3 scaling parameters for the colors of the image.
    Output:  Corresponding 3x3 color scaling matrix.
    '''
    D = {'r','g','b'}
    f = {}
    f[('r','r')] = scale_r
    f[('g','g')] = scale_g
    f[('b','b')] = scale_b
    return Mat((D,D), f)


## Task 9
def grayscale():
    '''
    Input: None
    Output: 3x3 greyscale matrix.
    '''
    D = {'r','g','b'}
    f = {}
    f[('r','r')] = 77/256
    f[('g','r')] = 77/256
    f[('b','r')] = 77/256

    f[('r','g')] = 151/256
    f[('g','g')] = 151/256
    f[('b','g')] = 151/256

    f[('r','b')] = 28/256
    f[('g','b')] = 28/256
    f[('b','b')] = 28/256

    return Mat((D,D), f)

## Task 10
def reflect_about(p1,p2):
    '''
    Input: 2 points that define a line to reflect about.
    Output:  Corresponding 3x3 reflect about matrix.
    '''
    pass


