from hw3 import *
from vec import *
from mat import *
v = Vector('4 3 2 1')
A = Matrix('-5 10; -4 8; -3 6; -2 4')
a = dot_product_vec_mat_mult(v, A)

A = Matrix('-1 1 2; 1 2 3; 2 2 1')
B = Matrix('-1 1 2; 1 2 3; 2 2 1')
a = dot_prod_mat_mat_mult(A, B)
print(a)
