from vec import Vec

def Matrix(L):
  byRow = L.split(";")
  matrix = []
  for r in byRow:
    matrix.append(r.split())
  rows = len(matrix)
  cols = len(matrix[0])
  D0 = set(range(rows))
  D1 = set(range(cols))
  f = {}
  for r in range(rows):
    for c in range(cols):
      f[(r,c)] = float(matrix[r][c])
  return Mat((D0, D1), f)

def Matrix0(matrix):
  rows = len(matrix)
  cols = len(matrix[0])
  D0 = set(range(rows))
  D1 = set(range(cols))
  f = {}
  for r in range(rows):
    for c in range(cols):
      f[(r,c)] = float(matrix[r][c])
  return Mat((D0, D1), f)

def getitem(M, k):
    assert k[0] in M.D[0] and k[1] in M.D[1]
    if k in M.f:
      return M.f[k]
    else:
      return 0

def setitem(M, k, val):
    assert k[0] in M.D[0] and k[1] in M.D[1]
    M.f[k] = val
    return

def add(A, B):
    assert A.D == B.D
    newF = {}
    for m in A.D[0]:
      for n in A.D[1]:
        newF[(m,n)] = A[m, n] + B[m, n]

    return Mat(A.D, newF)

def scalar_mul(M, alpha):
    newF = {}
    for m in M.D[0]:
      for n in M.D[1]:
        newF[(m,n)] = alpha * M[m, n]

    return Mat(M.D, newF)

def equal(A, B):
    assert A.D == B.D
    for m in A.D[0]:
      for n in A.D[1]:
        if A[m, n] != B[m, n]:
          return False
    return True

def transpose(M):
    newF = {}
    for m in M.D[0]:
      for n in M.D[1]:
        newF[(n, m)] = M[m, n]
    return Mat((M.D[1], M.D[0]), newF)

def vector_matrix_mul(v, M):
    ret = {}
    tmp = {}
    assert M.D[0] == v.D
    for n in M.D[1]:
      for m in M.D[0]:
        tmp[m] = M[m, n]
      ret[n] = v * Vec(v.D, tmp)

    return Vec(M.D[1], ret)

def matrix_vector_mul(M, v):
    assert M.D[1] == v.D
    ret = {}
    tmp = {}
    for m in M.D[0]:
      for n in M.D[1]:
        tmp[n] = M[m, n]
      ret[m] = v * Vec(v.D, tmp)

    return Vec(M.D[0], ret)

def matrix_matrix_mul(A, B):
    assert A.D[1] == B.D[0]
    aVecs = {}    
    bVecs = {}
    for r in A.D[0]:
      tmp = {}
      for c in A.D[1]:
        tmp[c] = A[r, c]
      aVecs[r] = Vec(A.D[1], tmp)

    for c in B.D[1]:
      tmp = {}
      for r in B.D[0]:
        tmp[r] = B[r, c]
      bVecs[c] = Vec(B.D[0], tmp)

    res = {}
    for k0, a in aVecs.items():
      for k1, b in bVecs.items():
        res[(k0, k1)] = a * b

    return Mat((A.D[0], B.D[1]), res)        


################################################################################

class Mat:
    def __init__(self, labels, function):
        self.D = labels
        self.f = function

    __getitem__ = getitem
    __setitem__ = setitem
    transpose = transpose

    def __neg__(self):
        return (-1)*self

    def __mul__(self,other):
        if Mat == type(other):
            return matrix_matrix_mul(self,other)
        elif Vec == type(other):
            return matrix_vector_mul(self,other)
        else:
            return scalar_mul(self,other)
            #this will only be used if other is scalar (or not-supported). mat and vec both have __mul__ implemented

    def __rmul__(self, other):
        if Vec == type(other):
            return vector_matrix_mul(other, self)
        else:  # Assume scalar
            return scalar_mul(self, other)

    __add__ = add

    def __sub__(a,b):
        return a+(-b)

    __eq__ = equal

    def ll(self):
      tmp = '['

      for m in self.D[0]:
        tmp += '['
        for n in self.D[1]:
          tmp += str(self[m, n]) + ","

        tmp = tmp[0:-1]
        tmp += '],'

      tmp = tmp[0:-1]
      tmp += ']'
      print(tmp)

    def toString(self):
      for m in self.D[0]:
        tmp = ''
        for n in self.D[1]:
          tmp += str(self[m, n]) + "\t"
        print(tmp)

    def copy(self):
        return Mat(self.D, self.f.copy())

    def __str__(M, rows=None, cols=None):
        "string representation for print()"
        if rows == None:
            try:
                rows = sorted(M.D[0])
            except TypeError:
                rows = sorted(M.D[0], key=hash)
        if cols == None:
            try:
                cols = sorted(M.D[1])
            except TypeError:
                cols = sorted(M.D[1], key=hash)
        separator = ' | '
        numdec = 3
        pre = 1+max([len(str(r)) for r in rows])
        colw = {col:(1+max([len(str(col))] + [len('{0:.{1}G}'.format(M[row,col],numdec)) if isinstance(M[row,col], int) or isinstance(M[row,col], float) else len(str(M[row,col])) for row in rows])) for col in cols}
        s1 = ' '*(1+ pre + len(separator))
        s2 = ''.join(['{0:>{1}}'.format(c,colw[c]) for c in cols])
        s3 = ' '*(pre+len(separator)) + '-'*(sum(list(colw.values())) + 1)
        s4 = ''.join(['{0:>{1}} {2}'.format(r, pre,separator)+''.join(['{0:>{1}.{2}G}'.format(M[r,c],colw[c],numdec) if isinstance(M[r,c], int) or isinstance(M[r,c], float) else '{0:>{1}}'.format(M[r,c], colw[c]) for c in cols])+'\n' for r in rows])
        return '\n' + s1 + s2 + '\n' + s3 + '\n' + s4

    def pp(self, rows, cols):
        print(self.__str__(rows, cols))

    def __repr__(self):
        "evaluatable representation"
        return "Mat(" + str(self.D) +", " + str(self.f) + ")"
